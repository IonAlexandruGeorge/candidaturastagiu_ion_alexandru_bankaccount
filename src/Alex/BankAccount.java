package Alex;

public class BankAccount {
	private int accountNo;
	private int balance;
	private String name;
	private String email;
	private int phoneNo;
	
	public void setaccountNo(int accountNo){
		this.accountNo=accountNo;
	}
	public int getaccountNo(){
		return accountNo;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(int phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	public void deposit(int fund){
		System.out.println("Current funds are: " +this.balance);
		this.balance+=fund;
		System.out.println("New balance after deposit is: " +this.balance);
	}
	
	public void withdraw(int fund){
		if(this.balance - fund <=0){
			System.out.println("Ai incercat sa retragi suma: " +fund + " dar a intervenit urmatoarea eroare:");
			System.out.println("Fonduri insuficiente!");
			System.out.println("Current funds are: " +this.balance);
			
		}else {
		System.out.println("Current funds are: " +this.balance);
		this.balance-=fund;
		System.out.println("New balance after deposit is: " +this.balance);
		}
	}

}
